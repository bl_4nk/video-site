<nav class="navbar navbar-default" data-spy="affix" data-offset-top="220">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-nav-collapse" aria-expanded="false"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      <?php if(getcong('site_logo')): ?>
        <a class="navbar-brand" href="<?php echo e(URL::to('/')); ?>"> <img src="<?php echo e(URL::asset('upload/source/'.getcong('site_logo'))); ?>" alt="Site Logo"> </a>
      <?php else: ?>
        <a class="navbar-brand" href="<?php echo e(URL::to('/')); ?>"> <img src="<?php echo e(URL::asset('site_assets/images/template/logo.png')); ?>" alt="Site Logo"> </a>
      <?php endif; ?>

  </div>

  </div>
</nav>
<?php /**PATH C:\laragon\www\video_script\resources\views/_particles/header_clean.blade.php ENDPATH**/ ?>