@extends('site_app_clean')

@section('content')
    <div class="container-narrow" style="height: 100vh">
        <div class="col-md-6 col-md-offset-3" style="margin: 0; position: absolute; top: 50%; left: 50%; -ms-transform: translate(-50%, -50%); transform: translate(-50%, -50%);">
            <div class="panel" style="background-color: rgb(6, 1, 7)">
                <div class="panel-body">
                    <center>
                        <h1>Who's watching?</h1>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">User 1</h3>
                                    </div>
                                    <div class="panel-body">
                                        <img src="https://vignette.wikia.nocookie.net/despicableme/images/c/ca/Bob-from-the-minions-movie.jpg/" style="height: 190px; width: 100%;">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">User 2</h3>
                                    </div>
                                    <div class="panel-body">
                                        <img src="https://c4.wallpaperflare.com/wallpaper/772/393/77/deadpool-ryan-reynolds-deadpool-2-wallpaper-preview.jpg" style="height: 190px; width: 100%;">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Add User</h3>
                                    </div>
                                    <div class="panel-body">
                                        <img src="https://simpleicon.com/wp-content/uploads/add-user.svg" style="height: 190px; width: 100%;">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </center>
                </div>
            </div>
        </div>
    </div>
@endsection
