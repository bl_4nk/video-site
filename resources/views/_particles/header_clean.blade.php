<nav class="navbar navbar-default" data-spy="affix" data-offset-top="220">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-nav-collapse" aria-expanded="false"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      @if(getcong('site_logo'))
        <a class="navbar-brand" href="{{ URL::to('/') }}"> <img src="{{ URL::asset('upload/source/'.getcong('site_logo')) }}" alt="Site Logo"> </a>
      @else
        <a class="navbar-brand" href="{{ URL::to('/') }}"> <img src="{{ URL::asset('site_assets/images/template/logo.png') }}" alt="Site Logo"> </a>
      @endif

  </div>

  </div>
</nav>
